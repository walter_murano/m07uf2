<?php
include_once('../config/config.php');

class DBManager extends PDO{
    public $server = SERVER;
    public $user = USER;
    public $password = PASSWORD;
    public $db = DB;
    public $port = PORT;

    private $conexion;

    public function __construct(){
        $this->conectar();
    }

    private final function conectar(){
        $conexion = null;
        try{
            if(is_array(PDO::getAvailableDrivers())){
                if(in_array("pgsql",PDO::getAvailableDrivers())){
                    $conexion = new PDO("pgsql:host=$this->server; port=$this->port; dbname=$this->db; user=$this->user; password =$this->password");

                }else{
                    throw new PDOException('Error de conexión');
                }
            }
        }catch (PDOException $e){
            echo $e->getMessage();
        }

        $this->setConexion($conexion);
    }

    public final function getConexion()
    {
        return $this->conexion;
    }

    public function setConexion($conexion)
    {
        $this->conexion = $conexion;
    }

    public function cerrarConexion()
    {
        $this->conexion =null;
    }


}