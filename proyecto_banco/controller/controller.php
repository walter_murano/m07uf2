<?php

require_once ('../model/Cliente.php');
require_once ('../model/ClienteModel.php');
require_once ('../model/CuentaModel.php');
require_once ('../Helpers/helper.php');
require_once('../model/MovimientoModel.php');

if(isset($_POST['lang'])){
    $lang = $_POST['lang'];
    setcookie('lang', $lang, time()+60*60*24*30, '/', 'localhost');
    header('Location: ' . $_SERVER['HTTP_REFERER']);
}

if(isset($_POST['submit'])) {
    if ($_POST['control'] == 'register') {
        if (validateRegister()) {
            $cliente = new Cliente($_POST["name"], $_POST["surname"], $_POST["bornDate"], $_POST["gender"], $_POST["telf"], $_POST["dni"], $_POST["email"],"", $_POST["password"]);
            insertCliente($cliente);
            //redirijo  a login
            header('Location: ../views/login.php');
        } else {
            require_once('../views/register.php');
        }
    }

    if ($_POST['control'] == 'login') {
        $client = getUser($_POST['dni']);
        $hash = $client['password'];
        if (password_verify($_POST['password'], $hash)) {
            session_start();
            $cliente = new Cliente($client["nombre"], $client["apellidos"], $client["fecha_nacimiento"], $client["sexo"], $client["telefono"], $client["dni"], $client["email"], $client['imagen'],$client["password"]);
            $_SESSION['cliente']=serialize($cliente);
            header('Location: ../views/init.php');
        } else {
            require_once('../views/login.php');
        }
    }


    if ($_POST['control'] == 'profile') {
        session_start();
        $cliente=$_SESSION['cliente'];

        $check = getimagesize($_FILES['upload']['tmp_name']);
        $fileName = $_FILES['upload']['name'];
        echo $fileName . '<br/>';
        $fileSize = $_FILES['upload']['size'];
        $fileType = $_FILES['upload']['type'];
        $image = file_get_contents($_FILES['upload']['tmp_name']);
        $cliente->setImg($image);

        if ($check !== false) {
            error_log('init_profile');
            updateCliente($cliente);
            $data = getImage($cliente);
            ob_start();
            fpassthru($data);
            $im = ob_get_contents();
            ob_end_clean();
            echo "<br/><img src='data:image/*;base64," . base64_encode($im) . "'/>";

        }
    }

    if($_POST['control']=='create'){
        session_start();
        createAccount(unserialize($_SESSION['cliente'])->getDni());
        header("Location: ../views/init.php");
    }

    if($_POST['control']=='select_account'){
        $saldo = getSaldo($_POST['cuentas']);
        session_start();
        $_SESSION['lista'] = getMovimientos($_POST['cuentas']);
        $_SESSION['saldo']=$saldo;
        header("Location: ../views/query.php");

    }

    if($_POST['control']=='transfer') {
        if (existeCuenta($_POST['cuentas']) && existeCuenta($_POST['cuenta_destino'])) {
            if(quedaSaldo($_POST['cuentas'])){
                transfer($_POST['cuentas'], $_POST['cuenta_destino'],$_POST['cantidad']);
                header("Location: ../views/query.php");
            }else{
                echo 'Saldo insuficiente';
            }
        }


    }

    if($_POST['control']=='query') {
        session_start();
        $_SESSION['saldo']=getSaldo($_POST['cuentas']);
        $_SESSION['lista']=getMovimientos($_POST['cuentas']);
        header("Location: ../views/query.php");
    }
}