<?php
require_once ('../Helpers/DBManager.php');

function insertCliente($cliente){
    $manager = new DBManager();
    try{
        $sql= "INSERT INTO cliente (nombre, apellidos, fecha_nacimiento, sexo, telefono, dni, email, password) VALUES(:nombre, :apellidos, :fecha_nacimiento, :sexo, :telefono, :dni, :email, :password)";
        $nombre = $cliente->getNombre();
        $apellidos = $cliente->getApellidos();
        $fecha_nacimiento = $cliente->getFechaNacimiento();
        $sexo = $cliente->getSexo();
        $telefono = $cliente->getTelefono();
        $dni = $cliente->getDni();
        $email = $cliente->getEmail();
        $password = password_hash($cliente->getPassword(), PASSWORD_DEFAULT, ['cost'=>10]);


        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':nombre', $nombre);
        $stmt->bindParam(':apellidos', $apellidos );
        $stmt->bindParam(':fecha_nacimiento', $fecha_nacimiento);
        $stmt->bindParam(':sexo', $sexo);
        $stmt->bindParam(':telefono', $telefono);
        $stmt->bindParam(':dni', $dni);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':password', $password);

        if($stmt->execute()){
            echo 'Todo OK </br>';
        }else{
            echo 'Mal </br>';
        }


    } catch(PDOException $e){
        echo $e->getMessage();
    }
}
function updateCliente($cliente){
    $manager = new DBManager();
    try{
        $sql= "UPDATE cliente SET telefono=:telefono, dni=:dni, email=:email, imagen=:img WHERE dni = :dni";
        $password = password_hash($cliente->getPassword(), PASSWORD_DEFAULT, ['cost'=>10]);

        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindValue(':telefono', $cliente->getTelefono());
        $stmt->bindValue(':dni', $cliente->getDni());
        $stmt->bindValue(':email', $cliente->getEmail());
        $stmt->bindValue(':img',$cliente->getImg(),PDO::PARAM_LOB);
        //$stmt->bindValue(':password', $password);

        if($stmt->execute()){
            echo 'Todo OK </br>';
        }else{
            echo 'Mal </br>';
        }

    } catch(PDOException $e){
        echo $e->getMessage();
    }
}

function getUser($dni){
    $conexion = new DBManager();

    try{
        $sql = "SELECT * FROM cliente WHERE dni = :dni";
        $stmt= $conexion ->getConexion()->prepare($sql);
        $stmt->bindParam(':dni', $dni);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result[0];
    }catch(PDOException $e){

    }
}

function getImage($cliente){

    $manager = new DBManager();
    try{
        $sql = 'SELECT imagen FROM cliente WHERE dni=:dni';
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindValue(':dni',unserialize($cliente)->getDni());
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result[0]['imagen'];
    }catch (PDOException $e){
        echo $e->getMessage();
    }
}
